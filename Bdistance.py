#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 18:21:43 2022

@author: franci
"""

import numpy as np
import persim
from persim import bottleneck_matching, bottleneck
import tadasets
import ripser
from ripser import ripser
import matplotlib.pyplot as plt
from math import ceil


data0=np.loadtxt("/Users/franci/Desktop/omologia/data_00.txt")
data1=np.loadtxt("/Users/franci/Desktop/omologia/data_01.txt")
data2=np.loadtxt("/Users/franci/Desktop/omologia/data_02.txt")
data3=np.loadtxt("/Users/franci/Desktop/omologia/data_03.txt")
data4=np.loadtxt("/Users/franci/Desktop/omologia/data_04.txt")
Dati=[data0, data1,data2, data3, data4]

dgms0 = ripser(data0)['dgms']
dgms1 = ripser(data1)['dgms']
dgms2 = ripser(data2)['dgms']
dgms3 = ripser(data3)['dgms']
dgms4 = ripser(data4)['dgms']
DGMS=[dgms0, dgms1, dgms2, dgms3, dgms4]
H1=[0,0,0,0,0]
H0=[0,0,0,0,0]


#Bottleneck distance
for i in range(0,5):
    for j in range(i+1,5):                       
        print("tra i dati", i," e ",j)
        for dim, (I1, I2) in enumerate(zip(DGMS[i], DGMS[j])):
            bdist = bottleneck(I1, I2)          
            print("H%i bottleneck distance: %.3g"%(dim, bdist))
            
          
        
#Generate  diagrams for each of the data sets        
for i in range (0,5):
    H1[i]=ripser(Dati[i])['dgms'][1]
    H0[i]=ripser(Dati[i])['dgms'][0]
    
#Bottolneck per H1 diagrammi 
  
for i in range (0,5):
    
    for j in range (i+1, 5): 
        plt.figure() 
        bn_matching, (matchidx, dist) = persim.bottleneck(H1[i], H1[j], matching=True)
        fig=bottleneck_matching(H1[i], H1[j], matchidx, dist) 
         
      

#per H0, non troppo utili             
"""for i in range (0,5):
    
    for j in range (i+1, 5): 
        plt.figure()
        bn_matching, (matchidx, dist) = persim.bottleneck(H0[i], H0[j], matching=True)
        fig=bottleneck_matching(H0[i], H0[j], matchidx, dist) 
 """      
             
        
        



