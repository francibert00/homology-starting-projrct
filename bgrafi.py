"""
#per importare dati
data0=np.loadtxt("./data_00.txt")
data1=np.loadtxt("./data_01.txt")
data2=np.loadtxt("./data_02.txt")
data3=np.loadtxt("./data_03.txt")
data4=np.loadtxt("./data_04.txt")


dgms0 = ripser(data0)['dgms']
dgms1 = ripser(data1)['dgms']
dgms2 = ripser(data2)['dgms']
dgms3 = ripser(data3)['dgms']"""
import numpy as np
import persim
from persim import bottleneck_matching
import tadasets
import ripser
import matplotlib.pyplot as plt
data_clean = tadasets.dsphere(d=1, n=100, noise=0.0)
data_noisy = tadasets.dsphere(d=1, n=100, noise=0.1)
plt.scatter(data_clean[:,0], data_clean[:,1], label="clean data")
plt.scatter(data_noisy[:,0], data_noisy[:,1], label="noisy data")
plt.axis('equal')
plt.legend()
plt.show()
#../_images/notebooks_distances_3_0.png
#Generate  diagrams for each of the data sets
dgm_clean = ripser.ripser(data_clean)['dgms'][1]
dgm_noisy = ripser.ripser(data_noisy)['dgms'][1]
persim.plot_diagrams([dgm_clean, dgm_noisy], labels=['Clean $H_1$', 'Noisy $H_1$'])
bn_matching, (matchidx, D) = persim.bottleneck(dgm_clean, dgm_noisy, matching=True)
bottleneck_matching(dgm_clean, dgm_noisy, matchidx, D, labels=['Clean $H_1$', 'Noisy $H_1$'], ax=None)
plt.show()

#../_images/notebooks_distances_9_0.png
#The default option of matching=False will return just the distance if that is all you’re interested in.

print(D)
persim.bottleneck(dgm_clean, dgm_noisy)
#0.2810312509536743
#0.2810312509536743
